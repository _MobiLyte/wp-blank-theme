<?php /* Template Name: Demo Page Template */ get_header(); ?>

	<main role="main">
		<!-- section -->
		<section>

			<h1><?php the_title(); ?></h1>
			<p>
				<?php 
				$_SESSION["favcolor"] = "green";
				$_SESSION["favanimal"] = "cat";
				echo "Session variables are set.";
				



				 ?>
			 </p>

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php the_content(); ?>

				<?php comments_template( '', true ); // Remove if you don't want comments ?>

				<br class="clear">

				<?php edit_post_link(); ?>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'Wordprss Blank Theme' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

		</section>
		<!-- /section -->
	</main>

	<!-- GET CUSTOM WIDGET  -->
	<?php if ( is_active_sidebar( 'widget-area-1' ) ) : ?> <!-- DISPLAY WIDGET BY ID -->
		<ul id="sidebar">
			<?php dynamic_sidebar( 'widget-area-1' ); ?>
		</ul>
	<?php endif; ?>
	<!-- /GET CUSTOM WIDGET -->


<?php get_sidebar(); ?>

<?php get_footer(); ?>
