<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
	<body <?php body_class(); ?>>

		<!-- wrapper -->
		<div class="wrapper">

			<!-- header -->
			<header class="header clear" role="banner">

					<!-- logo -->
					<div class="logo">
						<a href="<?php echo home_url(); ?>">

						<?php 
							#php code to Get Custom logo
							$custom_logo_id = get_theme_mod( 'custom_logo' );
							$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
							if ( has_custom_logo() ) {
							echo '<img src="'. esc_url( $logo[0] ) .'">';
							} else {
							echo '<h1>'. get_bloginfo( 'name' ) .'</h1>';
							}
						?>

							<!-- Get logo image -->
							<img src="<?php echo get_template_directory_uri(); ?>assets/img/logo.svg" alt="Logo" class="logo-img">
						</a>
					</div>
					<!-- /logo -->

					<!-- nav -->
					<nav class="nav" role="navigation">
						<?php wp_blank_nav(); ?>
					</nav>
					<!-- /nav -->
					<!-- alternate way to get nav nenu -->
					<?php
							$args = array( 
						  	'theme_location' => 'header-menu',       
						   	'menu' => 'Header Menu',
						   	'menu_class'     => 'nav navbar-nav navbar-right',
							);
							wp_nav_menu( $args ); 
							
					?>

			</header>
			<!-- /header -->
